import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

class MyStack implements StackOperations {

    List<String> stack = new LinkedList<>();

    @Override
    public List<String> get() {
        return stack;
    }

    @Override
    public Optional<String> pop() {

        if (stack.isEmpty()) {
            return Optional.empty();
        } else {

            final Optional<String> RETURNED_ITEM = Optional.of(stack.get(0));
            stack.remove(0);
            return RETURNED_ITEM;
        }
    }

    @Override
    public void push(final String item) {
        stack.add(0, item);
    }
}